# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavacli.
#
# lavacli is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavacli is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavacli.  If not, see <http://www.gnu.org/licenses/>

import sys
import xmlrpc.client

from lavacli import main


def test_system_active(setup, monkeypatch, capsys):
    version = "2018.4"
    monkeypatch.setattr(sys, "argv", ["lavacli", "system", "active"])
    monkeypatch.setattr(xmlrpc.client.ServerProxy, "data", [{"request": "system.version", "args": (), "ret": version},
                                                            {"request": "scheduler.workers.list",
                                                             "args": (),
                                                             "ret": ["worker01", "worker02"]},
                                                            {"request": "scheduler.workers.update",
                                                             "args": ("worker01", None, "ACTIVE"),
                                                             "ret": None},
                                                            {"request": "scheduler.workers.update",
                                                             "args": ("worker02", None, "ACTIVE"),
                                                             "ret": None}])
    assert main() == 0
    assert capsys.readouterr()[0] == """Activate workers:
* worker01
* worker02
"""


def test_system_api(setup, monkeypatch, capsys):
    version = "2018.4"
    monkeypatch.setattr(sys, "argv", ["lavacli", "system", "api"])
    monkeypatch.setattr(xmlrpc.client.ServerProxy, "data", [{"request": "system.version", "args": (), "ret": version},
                                                            {"request": "system.api_version",
                                                             "args": (),
                                                             "ret": 2}])

    assert main() == 0
    assert capsys.readouterr()[0] == "2\n"


def test_system_export(setup, monkeypatch, capsys):
    # TODO: should be tested
    pass


def test_system_maintenance(setup, monkeypatch, capsys):
    # TODO: should be tested
    pass


def test_system_methods(setup, monkeypatch, capsys):
    # TODO: should be tested
    pass


def test_system_version(setup, monkeypatch, capsys):
    version = "2018.4"
    monkeypatch.setattr(sys, "argv", ["lavacli", "system", "version"])
    monkeypatch.setattr(xmlrpc.client.ServerProxy, "data", [{"request": "system.version", "args": (), "ret": version},
                                                            {"request": "system.version", "args": (), "ret": version}])

    assert main() == 0
    assert capsys.readouterr()[0] == "2018.4\n"


def test_system_whoami(setup, monkeypatch, capsys):
    version = "2018.4"
    monkeypatch.setattr(sys, "argv", ["lavacli", "system", "whoami"])
    monkeypatch.setattr(xmlrpc.client.ServerProxy, "data", [{"request": "system.version", "args": (), "ret": version},
                                                            {"request": "system.whoami", "args": (), "ret": "lava-admin"}])

    assert main() == 0
    assert capsys.readouterr()[0] == "lava-admin\n"


def test_system_whoami_anonymous(setup, monkeypatch, capsys):
    version = "2018.4"
    monkeypatch.setattr(sys, "argv", ["lavacli", "system", "whoami"])
    monkeypatch.setattr(xmlrpc.client.ServerProxy, "data", [{"request": "system.version", "args": (), "ret": version},
                                                            {"request": "system.whoami", "args": (), "ret": None}])

    assert main() == 0
    assert capsys.readouterr()[0] == "<AnonymousUser>\n"
